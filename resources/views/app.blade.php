<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.png') }}">
        <title>@yield('title')</title>
        @include('shared.scripts')
    </head>
    <body>
        @include('shared.header')
        @include('cookieConsent::index')
        @section('content')
        @show
        @include('shared.footer')
        @include('shared.bottomScript')
        @section('script')
        @show
    </body>
</html>

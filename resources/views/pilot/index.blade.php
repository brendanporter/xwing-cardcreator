@extends('app')

@section('title', 'pilot index')

@section('content')
    <form id="form-main">
        @csrf
        <div class="row">
            @include('pilot.formColumn1')
            @include('pilot.formColumn2')
            @include('pilot.formColumn3')
        </div>
    </form>
@endsection
@section('script')
    @include('pilot.script')
@endsection

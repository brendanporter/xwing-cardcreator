<div class="col s12 l4">

    <div>
        <div class="row">
            <div class="col s6">
                <label for="card-name">Card name</label>
                <input type="text" name="card-name">
            </div>
            <div class="col s6">
                <label for="limited">Limited <span id="limited-visualizer" class="badge" title="Limited"></span></label>
                <input type="range" id="limited" name="limited" min="1" max="4" value="4"/>
            </div>
        </div>

        <label for="pilot-title">Title</label>
        <input type="text" name="pilot-title">
    </div>
    <br/>

    <label for="faction">Faction</label>
    <div class="col s12" id="factionSelect">
        <div class="col m3 l1" symbolText="empire" title="Empire"><i
                class="xwing-miniatures-font xwing-miniatures-font-empire" symbolText="empire"></i></div>
        <div class="col m3 l1" symbolText="rebel" title="Rebel alliance"><i
                class="xwing-miniatures-font xwing-miniatures-font-rebel" symbolText="rebel"></i></div>
        <div class="col m3 l1" symbolText="scum" title="Scum"><i
                class="xwing-miniatures-font xwing-miniatures-font-scum" symbolText="scum"></i></div>
        <div class="col m3 l1" symbolText="firstorder" title="First order"><i
                class="xwing-miniatures-font xwing-miniatures-font-firstorder" symbolText="firstorder"></i></div>
        <div class="col m3 l1" symbolText="resistance" title="Resistance"><i
                class="xwing-miniatures-font xwing-miniatures-font-rebel-outline" symbolText="resistance"></i></div>
        <div class="col m3 l1" symbolText="csi" title="CSI"><i
                class="xwing-miniatures-font xwing-miniatures-font-firstorder" symbolText="csi"></i></div>
        <div class="col m3 l1" symbolText="republic" title="Republic"><i
                class="xwing-miniatures-font xwing-miniatures-font-rebel-outline" symbolText="republic"></i></div>
    </div>
    <br/>

    <label for="card-art-image">Card image</label>
    <div class="file-field input-field">
        <div class="btn">
            <span>Browse</span>
            <input type="file"/>
        </div>

        <div class="file-path-wrapper">
            <input name="card-art-image" class="file-path validate" type="text" placeholder="Upload file"/>
        </div>
    </div>
    <br/>

    <label for="initiative">Initiative <span id="initiative-visualizer" class="badge" title="Initiative"></span></label>
    <input type="range" name="initiative" min="1" max="6" value="1"/>

    <div>
        <div class="col s12">
            <label for="ability-select">Ability</label>
            <ul id="ability-select" class="tabs">
                <li class="tab col s3"><a class="active" href="#pilot-ability-div">Pilot</a></li>
                <li class="tab col s3"><a href="#ship-ability-div">Ship</a></li>
            </ul>
        </div>
        <div id="pilot-ability-div" class="col s12">
            <label for="pilot-ability">Pilot ability</label>
            <textarea rows="5" cols="50" name="pilot-ability"></textarea>
        </div>
        <div id="ship-ability-div" class="col s12">
            <label for="ship-ability">Ship ability</label>
            <textarea rows="5" cols="50" name="ship-ability"></textarea>
        </div>

        <p>** bold text **, * italic text *</p>
        <div class="row" id="selectIcons">
            <div class="col s12">
                <div class="col s1" symbolText="cha"><i class="xwing-miniatures-font xwing-miniatures-font-charge"
                                                        symbolText="cha"></i></div>
                <div class="col s1" symbolText="for"><i class="xwing-miniatures-font xwing-miniatures-font-forcecharge"
                                                        symbolText="for"></i></div>
                <div class="col s1" symbolText="hit"><i class="xwing-miniatures-font xwing-miniatures-font-hit"
                                                        symbolText="hit"></i></div>
                <div class="col s1" symbolText="crh"><i class="xwing-miniatures-font xwing-miniatures-font-crit"
                                                        symbolText="crh"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="foc"><i class="xwing-miniatures-font xwing-miniatures-font-focus"
                                                        symbolText="foc"></i></div>
                <div class="col s1" symbolText="cal"><i class="xwing-miniatures-font xwing-miniatures-font-calculate"
                                                        symbolText="cal"></i></div>
                <div class="col s1" symbolText="tlk"><i class="xwing-miniatures-font xwing-miniatures-font-lock"
                                                        symbolText="tlk"></i></div>
                <div class="col s1" symbolText="boo"><i class="xwing-miniatures-font xwing-miniatures-font-boost"
                                                        symbolText="boo"></i></div>
                <div class="col s1" symbolText="bar"><i class="xwing-miniatures-font xwing-miniatures-font-barrelroll"
                                                        symbolText="bar"></i></div>
                <div class="col s1" symbolText="eva"><i class="xwing-miniatures-font xwing-miniatures-font-evade"
                                                        symbolText="eva"></i></div>
                <div class="col s1" symbolText="rei"><i class="xwing-miniatures-font xwing-miniatures-font-reinforce"
                                                        symbolText="rei"></i></div>
                <div class="col s1" symbolText="rel"><i class="xwing-miniatures-font xwing-miniatures-font-reload"
                                                        symbolText="rel"></i></div>
                <div class="col s1" symbolText="rot"><i class="xwing-miniatures-font xwing-miniatures-font-rotatearc"
                                                        symbolText="rot"></i></div>
                <div class="col s1" symbolText="jam"><i class="xwing-miniatures-font xwing-miniatures-font-jam"
                                                        symbolText="jam"></i></div>
                <div class="col s1" symbolText="clk"><i class="xwing-miniatures-font xwing-miniatures-font-cloak"
                                                        symbolText="clk"></i></div>
                <div class="col s1" symbolText="coo"><i class="xwing-miniatures-font xwing-miniatures-font-coordinate"
                                                        symbolText="coo"></i></div>
                <div class="col s1" symbolText="sla"><i class="xwing-miniatures-font xwing-miniatures-font-slam"
                                                        symbolText="sla"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="m01"><i class="xwing-miniatures-font xwing-miniatures-font-straight"
                                                        symbolText="m01"></i></div>
                <div class="col s1" symbolText="m02"><i class="xwing-miniatures-font xwing-miniatures-font-bankleft"
                                                        symbolText="m02"></i></div>
                <div class="col s1" symbolText="m03"><i class="xwing-miniatures-font xwing-miniatures-font-bankright"
                                                        symbolText="m03"></i></div>
                <div class="col s1" symbolText="m04"><i class="xwing-miniatures-font xwing-miniatures-font-turnleft"
                                                        symbolText="m04"></i></div>
                <div class="col s1" symbolText="m05"><i class="xwing-miniatures-font xwing-miniatures-font-turnright"
                                                        symbolText="m05"></i></div>
                <div class="col s1" symbolText="m06"><i class="xwing-miniatures-font xwing-miniatures-font-kturn"
                                                        symbolText="m06"></i></div>
                <div class="col s1" symbolText="m07"><i class="xwing-miniatures-font xwing-miniatures-font-stop"
                                                        symbolText="m07"></i></div>
                <div class="col s1" symbolText="m08"><i class="xwing-miniatures-font xwing-miniatures-font-sloopleft"
                                                        symbolText="m08"></i></div>
                <div class="col s1" symbolText="m09"><i class="xwing-miniatures-font xwing-miniatures-font-sloopright"
                                                        symbolText="m09"></i></div>
                <div class="col s1" symbolText="m10"><i
                        class="xwing-miniatures-font xwing-miniatures-font-dalan-bankleft" symbolText="m10"></i></div>
                <div class="col s1" symbolText="m11"><i
                        class="xwing-miniatures-font xwing-miniatures-font-dalan-bankright" symbolText="m11"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="m12"><i class="xwing-miniatures-font xwing-miniatures-font-trollleft"
                                                        symbolText="m12"></i></div>
                <div class="col s1" symbolText="m13"><i class="xwing-miniatures-font xwing-miniatures-font-trollright"
                                                        symbolText="m13"></i></div>
                <div class="col s1" symbolText="m14"><i
                        class="xwing-miniatures-font xwing-miniatures-font-ig88d-sloopleft" symbolText="m14"></i></div>
                <div class="col s1" symbolText="m15"><i
                        class="xwing-miniatures-font xwing-miniatures-font-ig88d-sloopright" symbolText="m15"></i></div>
                <div class="col s1" symbolText="m16"><i
                        class="xwing-miniatures-font xwing-miniatures-font-reversestraight" symbolText="m16"></i></div>
                <div class="col s1" symbolText="m17"><i
                        class="xwing-miniatures-font xwing-miniatures-font-reversebankleft" symbolText="m17"></i></div>
                <div class="col s1" symbolText="m18"><i
                        class="xwing-miniatures-font xwing-miniatures-font-reversebankright" symbolText="m18"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="ept"><i class="xwing-miniatures-font xwing-miniatures-font-talent"
                                                        symbolText="ept"></i></div>
                <div class="col s1" symbolText="for"><i class="xwing-miniatures-font xwing-miniatures-font-forcepower"
                                                        symbolText="for"></i></div>
                <div class="col s1" symbolText="ast"><i class="xwing-miniatures-font xwing-miniatures-font-astromech"
                                                        symbolText="ast"></i></div>
                <div class="col s1" symbolText="tor"><i class="xwing-miniatures-font xwing-miniatures-font-torpedo"
                                                        symbolText="tor"></i></div>
                <div class="col s1" symbolText="mis"><i class="xwing-miniatures-font xwing-miniatures-font-missile"
                                                        symbolText="mis"></i></div>
                <div class="col s1" symbolText="can"><i class="xwing-miniatures-font xwing-miniatures-font-cannon"
                                                        symbolText="can"></i></div>
                <div class="col s1" symbolText="tur"><i class="xwing-miniatures-font xwing-miniatures-font-turret"
                                                        symbolText="tur"></i></div>
                <div class="col s1" symbolText="dev"><i class="xwing-miniatures-font xwing-miniatures-font-device"
                                                        symbolText="dev"></i></div>
                <div class="col s1" symbolText="cre"><i class="xwing-miniatures-font xwing-miniatures-font-crew"
                                                        symbolText="cre"></i></div>
                <div class="col s1" symbolText="gun"><i class="xwing-miniatures-font xwing-miniatures-font-gunner"
                                                        symbolText="gun"></i></div>
                <div class="col s1" symbolText="sen"><i class="xwing-miniatures-font xwing-miniatures-font-sensor"
                                                        symbolText="sen"></i></div>
                <div class="col s1" symbolText="ill"><i class="xwing-miniatures-font xwing-miniatures-font-illicit"
                                                        symbolText="ill"></i></div>
                <div class="col s1" symbolText="tit"><i class="xwing-miniatures-font xwing-miniatures-font-title"
                                                        symbolText="tit"></i></div>
                <div class="col s1" symbolText="mod"><i class="xwing-miniatures-font xwing-miniatures-font-modification"
                                                        symbolText="mod"></i></div>
                <div class="col s1" symbolText="con"><i class="xwing-miniatures-font xwing-miniatures-font-config"
                                                        symbolText="con"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="a01"><i class="xwing-miniatures-font xwing-miniatures-font-frontarc"
                                                        symbolText="a01"></i></div>
                <div class="col s1" symbolText="a02"><i class="xwing-miniatures-font xwing-miniatures-font-reararc"
                                                        symbolText="a02"></i></div>
                <div class="col s1" symbolText="a03"><i class="xwing-miniatures-font xwing-miniatures-font-fullfrontarc"
                                                        symbolText="a03"></i></div>
                <div class="col s1" symbolText="a04"><i class="xwing-miniatures-font xwing-miniatures-font-fullreararc"
                                                        symbolText="a04"></i></div>
                <div class="col s1" symbolText="a05"><i class="xwing-miniatures-font xwing-miniatures-font-bullseyearc"
                                                        symbolText="a05"></i></div>
                <div class="col s1" symbolText="a06"><i
                        class="xwing-miniatures-font xwing-miniatures-font-singleturretarc" symbolText="a06"></i></div>
                <div class="col s1" symbolText="a07"><i
                        class="xwing-miniatures-font xwing-miniatures-font-doubleturretarc" symbolText="a07"></i></div>
                <div class="col s1" symbolText="a08"><i class="xwing-miniatures-font xwing-miniatures-font-leftarc"
                                                        symbolText="a08"></i></div>
                <div class="col s1" symbolText="a09"><i class="xwing-miniatures-font xwing-miniatures-font-rightarc"
                                                        symbolText="a09"></i></div>
                <div class="col s1" symbolText="a10"><i
                        class="xwing-miniatures-font xwing-miniatures-font-rearbullseyearc" symbolText="a10"></i></div>
            </div>
        </div>
    </div>

    <br/>

    <label for="faction">Ship</label>
    <div class="row col s12">
        <div class="row col s12">
            <ul class="tabs">
                <li class="tab col m3 l1"><a href="#shipsEmpire"><i
                            class="xwing-miniatures-font xwing-miniatures-font-empire"></i></a></li>
                <li class="tab col m3 l1"><a href="#shipsRebel"><i
                            class="xwing-miniatures-font xwing-miniatures-font-rebel"></i></a></li>
                <li class="tab col m3 l1"><a href="#shipsScum"><i
                            class="xwing-miniatures-font xwing-miniatures-font-scum"></i></a></li>
                <li class="tab col m3 l1"><a href="#shipsFirstorder"><i
                            class="xwing-miniatures-font xwing-miniatures-font-firstorder"></i></a></li>
                <li class="tab col m3 l1"><a href="#shipsResistance"><i
                            class="xwing-miniatures-font xwing-miniatures-font-rebel-outline"></i></a></li>
                <li class="tab col m3 l1"><a href="#shipsCsi"><i
                            class="xwing-miniatures-font xwing-miniatures-font-firstorder"></i></a></li>
                <li class="tab col m3 l1"><a href="#shipsRepublic"><i
                            class="xwing-miniatures-font xwing-miniatures-font-rebel-outline"></i></a></li>
                <li class="tab col m3 l1"><a href="#shipsCustom"><i class="material-icons dp48">edit</i></a></li>
            </ul>
        </div>


        <div id="shipSelect">
            <div id="shipsEmpire" class="row col s12">
                <div class="col s1 selectableClick" symbolText="tiefighter"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-tiefighter" symbolText="tiefighter"></i>
                </div>
                <div class="col s1 selectableClick" symbolText="tieinterceptor"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-tieinterceptor"
                        symbolText="tieinterceptor"></i></div>
                <div class="col s1 selectableClick" symbolText="tieadvanced"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-tieadvanced" symbolText="tieadvanced"></i>
                </div>
            </div>
            <div id="shipsRebel" class="row col s12">
                <div class="col s1 selectableClick" symbolText="xwing"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-xwing" symbolText="xwing"></i></div>
                <div class="col s1 selectableClick" symbolText="ywing"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-ywing" symbolText="ywing"></i></div>
            </div>
            <div id="shipsScum" class="row col s12">
                <div class="col s1 selectableClick" symbolText="firespray31"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-firespray31" symbolText="firespray31"></i>
                </div>
                <div class="col s1 selectableClick" symbolText="protectoratestarfighter"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-protectoratestarfighter"
                        symbolText="protectoratestarfighter"></i></div>
            </div>
            <div id="shipsFirstorder" class="row col s12">
                <div class="col s1 selectableClick" symbolText="tiefofighter"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-tiefofighter" symbolText="tiefofighter"></i>
                </div>
            </div>
            <div id="shipsResistance" class="row col s12">
                <div class="col s1 selectableClick" symbolText="t70xwing"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-t70xwing" symbolText="t70xwing"></i></div>
            </div>
            <div id="shipsCsi" class="row col s12">
                <div class="col s1 selectableClick" symbolText="tiefofighter"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-tiefofighter" symbolText="tiefofighter"></i>
                </div>
            </div>
            <div id="shipsRepublic" class="row col s12">
                <div class="col s1 selectableClick" symbolText="t70xwing"><i
                        class="xwing-miniatures-ship xwing-miniatures-ship-t70xwing" symbolText="t70xwing"></i></div>
            </div>
            <div id="shipsCustom" class="row col s12">
                <div class="col s12">
                    <label for="ship-name-custom">Ship name</label>
                    <input type="text" name="ship-name-custom">
                </div>
                <label for="ship-image-custom">Ship logo</label>
                <div class="col s12 file-field input-field">
                    <div class="btn">
                        <span>Browse</span>
                        <input type="file"/>
                    </div>

                    <div class="file-path-wrapper">
                        <input name="ship-image-custom" class="file-path validate" type="text"
                               placeholder="Upload file"/>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

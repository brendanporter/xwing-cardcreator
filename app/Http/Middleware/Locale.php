<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Locale
{
    /**
     * Handle an incoming request.
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->getMethod() === 'GET' && explode('/', $request->getRequestUri())[1] !== 'api') {
            $locale = explode('/', $request->getRequestUri())[1];
            if (empty($locale)) {
                $locale = Config::get('app.locale');
            }
            if (!isset(Config::get('app.locales')[$locale])) {
                throw new NotFoundHttpException();
            }
            App::setLocale($locale);
        }
        return $next($request);
    }
}
